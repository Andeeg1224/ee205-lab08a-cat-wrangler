///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file unittest.cpp
/// @version 1.0
///
/// Unit Test File
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   @08_Apr_2021
///////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include "node.hpp"
#include "list.hpp"

using namespace std;
int main(){
   cout << "Hello World" << endl;
   Node node; //Instantiate a node
   DoubleLinkedList list; //Instantiate a DoubleLinkedList
   
   cout << "The list is empty befor putting in tests: " << boolalpha << list.empty() << endl;
   
   Node* t1 = new Node;
   Node* t2 = new Node;
   Node* t3 = new Node;
   Node* t4 = new Node;
   Node* t5 = new Node;
   Node* t6 = new Node;
   cout << "Test1: " << t1 << "  Test2: " << t2 << "  Test3: " << t3 << " Test4: " << t4 << " Test5: " << t5 << " Test6(only used for insert_after/Insert_before): " << t6 << endl;
   cout << endl;
   //Tests the functions to make sure each of them work well
   /*list.push_front(t1);
   cout << "The initial front of list after pushing test 1 to front is: " << list.get_first() << endl;
   list.push_front(t2);
   list.push_front(t3);
   cout << "The list is still empty after inputting 3 tests: " << boolalpha << list.empty() << endl;
   cout << "The first node from the list after pushing test 2 then test 3 to the front is: " << list.get_first() << endl;
   list.swap(t1, t3);
   cout << "After swapping back test1 and test 3 front node is: " <<list.get_first() << endl;
   cout << "The node following the node test 2 is: " << list.get_next ( t2 ) << endl;
   list.push_back(test4);
   cout << "After pushing new Node test4 to the back the last node is: " << list.get_last() << endl;
   cout << "The prev node to test4 is (should be test3): " << list.get_prev (t4) << endl;
   cout << "Before using pop front the amount of Nodes is: " << list.size() << endl;
   list.pop_front();
   cout << "After using pop front the amount of Nodes is: " << list.size() << endl;
   cout << "Before using pop back the amount of Nodes is: " << list.size() << endl;
   list.pop_back();
   cout << "After using pop back the amount of Nodes is: " << list.size() << endl;
   list.pop_back() */
   


   list.push_front(t5);
   list.push_front(t4);
   list.push_front(t3);
   list.push_front(t2);
   list.push_front(t1);
   //Test Each of the insert cases individually
   /*
   list.insert_after( t5, t6 );
   cout << "inserting t6 after t5 tail is: " << list.get_last() << " prev to tail is: " << list.get_prev(list.get_last()) << endl;
   
   list.insert_after( t3, t6 );
   cout << "inserting t6 after t3 prev to t4 is: " << list.get_prev(t4) << " prev to that is: " << list.get_prev(list.get_prev(t4)) << endl;

   list.insert_before( t1, t6 );
   cout << "inserting t6 before t1 head is: " << list.get_first() << " next to that is: " << list.get_next(list.get_first()) << endl;

   list.insert_before( t2, t6 );
   cout << "inserting t6 before t2 after t1 is: " << list.get_next(t1) << " next to that is: " << list.get_next(list.get_next(t1)) << endl;
*/
   cout<< "Resets list after checking each swap" << endl;
   list.swap( t2, t4 );
   cout << "After swapping t2/t4 t2 prev: " << list.get_prev(t2) << " t2next: " << list.get_next(t2) <<endl;
   cout << "t4prev: " << list.get_prev(t4) << " t4next: " << list.get_next(t4) << endl;
   cout << "4th is: " << list.get_prev(list.get_last()) << " second is: " << list.get_next(list.get_first()) << endl;
   swap( t2, t4 );
   cout << endl;

   list.swap( t4, t2 );
   cout << "After swapping t4/t2 t2 prev: " << list.get_prev(t2) << " t2next: " << list.get_next(t2) <<endl;
   cout << "t4prev: " << list.get_prev(t4) << " t4next: " << list.get_next(t4) << endl;
   cout << "4th is: " << list.get_prev(list.get_last()) << " second is: " << list.get_next(list.get_first()) << endl;
   swap( t4, t2 );
   cout << endl;
   
   list.swap( t1, t5 );
   cout << "Resetting the list then swap t1/t5 t1prev: " << list.get_prev(t1) << " t1next: " << list.get_next(t1) << endl;
   cout << "t5prev: " << list.get_prev(t5) << " t5next: " << list.get_next(t5) << endl;
   cout << "last is: " << list.get_last()  << " first is: " << list.get_first() << endl;
   list.swap( t1, t5 );
   cout << endl;
   
   list.swap( t5, t1 );
   cout << "Resetting the list then swap t5/t1 t1prev: " << list.get_prev(t1) << " t1next: " << list.get_next(t1) << endl;
   cout << "t5prev: " << list.get_prev(t5) << " t5next: " << list.get_next(t5) << endl;
   cout << "last is: " << list.get_last() << " first is: " << list.get_first() << endl;
   list.swap( t5, t1 );
   cout << endl; 

   list.swap( t1, t4 );
   cout << "Swapping t1/t4 t1prev: " << list.get_prev(t1) << " t1next: " << list.get_next(t1) << endl;
   cout << "t4prev: " << list.get_prev(t4) << " t4next: " << list.get_next(t4) << endl;
   cout << "first is: " << list.get_first() << " 4th is: " << list.get_prev( list.get_last() ) << endl;
   list.swap ( t1, t4 );
   cout << endl;

   list.swap( t4, t1 );
   cout << "Swapping t4/t1 t1prev: " << list.get_prev(t1) << " t1next: " << list.get_next(t1) << endl;
   cout << "t4prev: " << list.get_prev(t4) << " t4next: " << list.get_next(t4) << endl;
   cout << "first is: " << list.get_first() << " 4th is: " << list.get_prev( list.get_last() ) << endl;
   list.swap ( t4, t1 );
   cout << endl;

   list.swap( t2, t5 );
   cout << "Swapping t2/t5 t2prev: " << list.get_prev(t2) << " t2next: " << list.get_next(t2) << endl;
   cout << "t5prev: " << list.get_prev(t5) << " t5next: " << list.get_next(t5) << endl;
   cout << "2nd is: " << list.get_next(list.get_first()) << " 5th is: " << list.get_last() << endl;
   list.swap ( t2, t5 );
   cout << endl;
   
   list.swap( t5, t2 );
   cout << "Swapping t2/t5 t2prev: " << list.get_prev(t2) << " t2next: " << list.get_next(t2) << endl;
   cout << "t5prev: " << list.get_prev(t5) << " t5next: " << list.get_next(t5) << endl;
   cout << "2nd is: " << list.get_next(list.get_first()) << " 5th is: " << list.get_last() << endl;
   list.swap ( t5, t2 );
   cout << endl;
 
   list.swap( t1 , t2 );
   cout << "Swapping t1/t2 t1prev: " << list.get_prev(t1) << " t1next: " << list.get_next(t1) << endl;
   cout << "t2prev: " << list.get_prev(t2) << " t2next: " << list.get_next(t2) << endl;
   cout << "1st is: " << list.get_first() << " 2nd is: " << list.get_next(list.get_first())  << endl;
   list.swap ( t1, t2 );
   cout << endl;

   list.swap( t2 , t1 );
   cout << "Swapping t2/t1 t1prev: " << list.get_prev(t1) << " t1next: " << list.get_next(t1) << endl;
   cout << "t2prev: " << list.get_prev(t2) << " t2next: " << list.get_next(t2) << endl;
   cout << "1st is: " << list.get_first() << " 2nd is: " << list.get_next(list.get_first())  << endl;
   list.swap ( t2, t1 );
   cout << endl;
   
   
   list.swap( t4 , t5 );
   cout << "Swapping t4/t5 t4prev: " << list.get_prev(t4) << " t4next: " << list.get_next(t4) << endl;
   cout << "t5prev: " << list.get_prev(t5) << " t5next: " << list.get_next(t5) << endl;
   cout << "4th is: " << list.get_prev(list.get_last()) << " 5th is: " << list.get_last()  << endl;
   list.swap ( t4, t5 );
   cout << endl;
    
   list.swap( t5 , t4 );
   cout << "Swapping t5/t4 t4prev: " << list.get_prev(t4) << " t4next: " << list.get_next(t4) << endl;
   cout << "t5prev: " << list.get_prev(t5) << " t5next: " << list.get_next(t5) << endl;
   cout << "4th is: " << list.get_prev(list.get_last()) << " 5th is: " << list.get_last()  << endl;
   list.swap ( t5, t4 );
   cout << endl;
   
      
   list.swap( t3 , t4 );
   cout << "Swapping t3/t4 t3prev: " << list.get_prev(t3) << " t3next: " << list.get_next(t3) << endl;
   cout << "t4prev: " << list.get_prev(t4) << " t4next: " << list.get_next(t4) << endl;
   cout << "4th is: " << list.get_prev(list.get_last()) << " 3rd is: " << list.get_prev(list.get_prev(list.get_last()))  << endl;
   list.swap ( t3, t4 );
   cout << endl;
   
   list.swap( t4 , t3 );
   cout << "Swapping t4/t3 t3prev: " << list.get_prev(t3) << " t3next: " << list.get_next(t3) << endl;
   cout << "t4prev: " << list.get_prev(t4) << " t4next: " << list.get_next(t4) << endl;
   cout << "4th is: " << list.get_prev(list.get_last()) << " 3rd is: " << list.get_prev(list.get_prev(list.get_last()))  << endl;
   list.swap ( t4, t3 ); 
   cout << endl;
   cout << "Nodes: " << list.size() << endl;
   
}

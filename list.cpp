///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file list.cpp
/// @version 1.0
///
/// cpp file for List
///
/// @author Andee Gary <andeeg@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   @08_Apr_2021
//////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include "list.hpp"
using namespace std;

   const bool DoubleLinkedList::empty() const{
      if ( head == nullptr )
         return true;
      else 
         return false;
      }
   
   void DoubleLinkedList::push_front( Node* newNode ){
      if ( newNode == nullptr )
         return; //If no New Node just exit
      
      if (head != nullptr){
         newNode-> next = head;
         newNode -> prev = nullptr;
         head ->prev = newNode;
         head = newNode;
      }
      else{
         newNode-> next = nullptr;
         newNode-> prev = nullptr;
         head = newNode;
         tail = newNode;
      }
      counter++;
      }
   
   Node* DoubleLinkedList::pop_front(){
      if ( head == nullptr )
         return nullptr;
      else if ( head == tail ){
         Node* popFront = head;
         head = nullptr;
         tail = nullptr;
         return popFront;
         }
      else{
         Node* popFront = head;
         head = head->next;
         head -> prev = nullptr;
         return popFront;
         }
      counter--;
   }
   
   Node* DoubleLinkedList::get_first() const {
      if ( head == nullptr )
         return nullptr;
      else
         return head;
   }
   
   Node* DoubleLinkedList::get_next( const Node* currentNode ) const {
      if ( currentNode -> next == nullptr )
         return nullptr;
      else
         return currentNode -> next;
   }
   
   void DoubleLinkedList::push_back( Node* newNode ){
      if ( newNode == nullptr )
         return;
      if ( tail != nullptr ) {
         newNode->next = nullptr;
         newNode-> prev = tail;
         tail-> next = newNode;
         tail = newNode;
      }
      else{
         newNode->next = nullptr;
         newNode->prev = nullptr;
         head = newNode;
         tail = newNode;
      }
      counter++;
      }

   Node* DoubleLinkedList::pop_back(){
      if( head == nullptr )
         return nullptr;
      else if ( head == tail ) {
      Node* popBack = tail; 
      head = nullptr;
      tail = nullptr;
      return popBack;
      }
      else{
      Node* popBack = tail;
      tail = tail-> prev;
      tail-> next = nullptr;
      return popBack;
   }
      counter--;
   }
   
   Node* DoubleLinkedList::get_last() const{
      if ( tail == nullptr )
         return nullptr;
      else
         return tail;
   }

   Node* DoubleLinkedList::get_prev ( const Node* currentNode ) const{
      if ( currentNode -> prev == nullptr )
         return nullptr;
      else
         return currentNode -> prev;
   }
   void DoubleLinkedList::insert_after( Node* currentNode, Node* newNode ){
      if ( newNode == nullptr ) 
         return;
      if ( currentNode-> next == nullptr ){
         currentNode->next = newNode;
         newNode->prev = currentNode;
         newNode->next = nullptr;
         tail = newNode;
      }
      else{
         newNode->next = currentNode->next;
         newNode->prev = currentNode;
         currentNode->next = newNode;
         newNode->next->prev = newNode;
      }
   } 

   void DoubleLinkedList::insert_before( Node* currentNode, Node* newNode ){
      if ( newNode == nullptr )
         return;
      if ( currentNode->prev == nullptr ){
         newNode->prev = nullptr;
         newNode->next = currentNode;
         currentNode->prev = newNode;
         head = newNode;
      }
      else{
         newNode->prev = currentNode->prev;
         newNode->next = currentNode;
         currentNode->prev = newNode;
         newNode->prev->next = newNode;
      }
   }
  
   void DoubleLinkedList::swap( Node* node1, Node* node2 ){
      //Nodes equal
      if( node1 == node2 )
         return;
      //Nodes both Null
      else if( node1 == nullptr ||  node2 == nullptr )
         return;
      //N1 = head/ N2 = tail
      else if( node1->prev == nullptr && node2->next == nullptr ){
         Node* swap = new Node;
         swap->next = node1->next;
         node1->next = nullptr;
         node1->prev = node2->prev;
         node2->prev = nullptr;
         node2->next = swap->next;
         node1->prev->next = node1;
         node2->next->prev = node2;
         tail = node1; 
         head = node2;
      }
      //N1=head N2 not tail
      else if( node1->prev == nullptr && node2->next != nullptr && node1->next != node2 ){
         Node*swap = new Node;
         swap->next = node1->next;
         node1->next = node2->next;
         node1->prev = node2->prev;
         node2->prev = nullptr;
         node2->next = swap->next;
         node1->prev->next = node1;
         node1->next->prev = node1;
         node2->next->prev = node2;
         head = node2;
      }
      //N1=tail n2 not head
      else if( node1->next == nullptr && node2->prev != nullptr && node1->prev != node2 ){
         Node* swap = new Node;
         swap->prev = node1->prev;
         node1->next = node2->next;
         node1->prev = node2->prev;
         node2->prev = swap->prev;
         node2->next = nullptr;
         node2->prev->next = node2;
         node1->next->prev = node1;
         node1->prev->next = node1;
         tail = node2;
      }
      //n1 tail n2 head
      else if( node1->next == nullptr && node2->prev == nullptr ){
         Node* swap = new Node;
         swap->prev = node1->prev;
         node1->prev = nullptr;
         node1->next = node2->next;
         node2->prev = swap->prev;
         node2->next = nullptr;
         node1->next->prev = node1;
         node2->prev->next = node2;
         tail = node2;
         head = node1;
      }
      //n2 head n1 not tail
      else if( node1->next != nullptr && node2->prev == nullptr && node2->next != node1 ){
         Node*swap = new Node;
         swap->next = node2->next;
         node2->next = node1->next;
         node2->prev = node1->prev;
         node1->prev = nullptr;
         node1->next = swap->next;
         node2->prev->next = node2;
         node2->next->prev = node2;
         node1->next->prev = node1;
         head = node1;
      }
      //n1 not head n2 is tail
      else if( node1->prev != nullptr && node2->next == nullptr && node2->prev != node1 ){
         Node* swap = new Node;
         swap->prev = node2->prev;
         node2->next = node1->next;
         node2->prev = node1->prev;
         node1->next = nullptr;
         node1->prev = swap->prev;
         node1->prev->next = node1;
         node2->next->prev = node2;
         node2->prev->next = node2;
         tail = node1;
      }
      //n1 is head n2 adjacent
      else if( node1->prev == nullptr && node1->next == node2 ){
         Node* swap = new Node;
         swap->next = node2->next;
         node2->prev = nullptr;
         node2->next = node1;
         node1->prev = node2;
         node1->next = swap->next;
         node1->next->prev = node1;
         head = node2;
      }
      //n2 is head n1 adjacent
      else if( node2->prev == nullptr && node2->next == node1 ){
         Node* swap = new Node;
         swap->next = node1->next;
         node1->prev = nullptr;
         node1->next = node2;
         node2->prev = node1;
         node2->next = swap->next;
         node2->next->prev = node2;
         head = node1;
      }
      //n1 tail n2 adjacent
      else if( node1->next == nullptr && node1->prev == node2 ){
         Node* swap = new Node;
         swap->prev = node2->prev;
         node2->next = nullptr;
         node2->prev = node1;
         node1->next = node2;
         node1->prev = swap->prev;
         node1->prev->next = node1;
         tail = node2;
      }
      //n2 tail n1 adjacent
      else if( node2->next == nullptr && node2->prev == node1 ){
         Node* swap = new Node;
         swap->prev = node1->prev;
         node1->next = nullptr;
         node1->prev = node2;
         node2->next = node1;
         node2->prev = swap->prev;
         node2->prev->next = node2;
         tail = node1;
      }
      //n1 before n2 adjacent
      else if( node1->next == node2 && node1->prev != nullptr && node2->next != nullptr ){
         Node* swap = new Node;
         swap->prev = node1->prev;
         swap->next = node1->next;
         node1->prev = node2;
         node1->next = node2->next;
         node2->prev = swap->prev;
         node2->next = node1;
         node2->prev->next = node2;
         node1->next->prev = node1;
      }
      //n2 before n1 adjacent
      else if( node1->prev == node2 && node1->next != nullptr && node2->prev != nullptr ){
         Node* swap = new Node;
         swap->prev = node1->prev;
         swap->next = node1->next;
         node1->next = node2;
         node1->prev = node2->prev;
         node2->prev = node1;
         node2->next = swap->next;
         node2->prev->next = node2;
         node1->next->prev = node1;
      }
      //n1 n1 not adjacent and not head or tail
      else{
         Node* swap = new Node;
         swap->next = node1->next;
         swap->prev = node1->prev;
         node1->next = node2->next;
         node1->prev = node2->prev;
         node2->prev = swap->prev;
         node2->next = swap->next;
         
         node1->next->prev = node1;
         node1->prev->next = node1;
         node2->prev->next = node2;
         node2->next->prev = node2;

      }
   }
   const bool DoubleLinkedList::isSorted() const { 
      if ( counter <=1 ) //Empty List or 1 item
         return true;
      for( Node* i = head; i ->next != nullptr ; i = i->next ) {
         if ( *i > *i->next )
            return false;
      }
      return true;
   }
   
   void DoubleLinkedList::insertionSort(){
      if ( counter <=1 )
         return;

      for( Node* i = head; i->next != nullptr; i = i->next ){
         Node* minNode = i;

         for( Node* j = i->next; j != nullptr; j = j->next ){
            if(*minNode > *j )
               minNode = j;
         }
         swap( i, minNode );
         i = minNode;
      }
   }
   


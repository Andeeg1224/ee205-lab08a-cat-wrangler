///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file list.hpp
/// @version 1.0
///
/// hpp file for list
///
/// @author Andee Gary <andeeg@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   @08_Apr_2021
//////////////////////////////////////////////////////////////////////////////
#pragma once
#include "node.hpp"

using namespace std;


   class DoubleLinkedList {
      protected:
         Node* head = nullptr;
         Node* tail = nullptr;
      public:
         int counter;
         const bool empty() const;
         void push_front( Node* newNode );
         Node* pop_front();
         Node* get_first() const;
         Node* get_next( const Node* currentNode ) const;
         inline unsigned int size() const{
            unsigned int count = 0;
            Node* currentNode = head; 
            while ( currentNode != nullptr ) {
               currentNode = currentNode -> next;
               count++;
            }
            return count;
            }
         void push_back ( Node* newNode );
         Node* pop_back();
         Node* get_last() const;
         Node* get_prev ( const Node* currentNode ) const;
         void insert_after ( Node* currentNode, Node* newNode );
         void insert_before ( Node* currentNode, Node* newNode );
         void swap ( Node* node1, Node* node2 );
         const bool isSorted() const;
         void insertionSort();
   };

